﻿using System;

namespace SpeedCodingThree
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Stuff stuff = new Stuff();
        }
    }

    class Stuff
    {
        //int[] IntArray = new int[] { 1, 2, 4, -2, -7, -5, 8 };          
        //int[] IntArray = new int[] { 5, 2, -7, 3, -2, 5, 4, -4 };       //-7
        int[] IntArray = new int[] { 5, 2, -7, 3, -2, 5, 7, 4, -4 };    //7
        public Stuff()
        {
            int big = 0;

            foreach(int i in IntArray)
            {
                int temp = Math.Abs(i);
                if (Math.Abs(big) < temp)
                {
                    big = i;
                }
                else if (Math.Abs(big) == temp)
                {
                    big = temp;
                }
            }

            Console.WriteLine("Größter Betrag ist: " + big);
            Console.ReadLine();
        }
    }
}
