﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdCamera : MonoBehaviour
{
    CharacterController charContr;

    Vector3 movDir;

    [SerializeField]
    private float MovSpeed = 0;

    float gravity = 20.0f;

    float yAxis = 0;
    [SerializeField]
    float ySpeed = 0;
    float xAxis = 0;
    [SerializeField]
    float xSpeed = 0;
    [SerializeField]
    Vector2 minMaxYAxis;

    public GameObject CamAnchor;
    // Start is called before the first frame update
    void Start()
    {
        charContr = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        RotateCharakter();
        MoveCharakter();
    }

    public void MoveCharakter()
    {
        movDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        movDir *= MovSpeed;
        movDir = transform.TransformDirection(movDir);

        movDir.y -= gravity * Time.deltaTime;

        charContr.Move(movDir * Time.deltaTime);
    }

    public void RotateCharakter()
    {
        yAxis += Input.GetAxis("Mouse X") * xSpeed * Time.deltaTime;
        transform.eulerAngles = new Vector3(0,yAxis,0);

        xAxis -= Input.GetAxis("Mouse Y") * ySpeed * Time.deltaTime;
        xAxis = Mathf.Clamp(xAxis, minMaxYAxis.x, minMaxYAxis.y);
        CamAnchor.transform.localEulerAngles = new Vector3(xAxis, 0, 0);
    }
}