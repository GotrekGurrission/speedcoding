﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SpeedCodingTwo
{
    /*
    Aufgabenstellungs Stuff
    Erster nicht doppelter Buchtstabe finden
    */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Stuff temp = new Stuff(Console.ReadLine());


            Console.ReadLine();
        }
    }

    class Stuff
    {
        public string input;
        char output;
        public Stuff(string _input)
        {
            input = _input.ToLower();
            Console.WriteLine(input);
            foreach(char c in input.ToCharArray())
            {
                if(Regex.Matches(input, Regex.Escape(c.ToString())).Count == 1)
                {
                    output = c;
                    Console.WriteLine("Der erste Buchstabe: " + output);
                    break;
                }
            }
        }
    }
}
